﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lab2.Controllers
{
    public class ExercisesController : Controller
    {
        public ContentResult SearchSequence(string word, string sentence)
        {
            string message = "Sentence " + sentence + " contains the word " + word;
            string message2 = "Sentence " + sentence + " does NOT contain the word " + word;
            string errorMessage = "A parameter is missing!";

            if(word != null && sentence != null)
            {
                if (sentence.Contains(word))
                {
                    return Content(message);
                }
                else
                {
                    return Content(message2);
                }
            }
            else
            {
                return Content(errorMessage);
            }
        }

        public ActionResult SearchSequenceOptional(string word, string? sentence)
        {
            ViewBag.message = "Sentence " + sentence + " contains the word " + word;

            if(sentence == null)
            {
                return HttpNotFound("Sentence parameter is missing!");
            }
            if (!sentence.Contains(word))
            {
                ViewBag.message = "Sentence " + sentence + " does NOT contains the word " + word;
            }
            return View();
        }

        public string NumberRegexParser(int? number)
        {
            return "the input number is: " + number.ToString();
        }

        [Route("LanguageRexedParser/{input:regex(^(a+)(b+)$)}")]
        public string RegexParser(string? input)
        {
            return "the input is: " + input;
        }
    }
}